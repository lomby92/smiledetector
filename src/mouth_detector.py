import numpy as np
import cv2

mouth_classifier = 'config/mouth.xml'

sentiment_happy = "Happy"
sentiment_sad = "Sad"

class mouth_detector:

    def __init__(self, sentiment_evaluator):
        self.mouth_cascade = cv2.CascadeClassifier(mouth_classifier)
        self.sentiment = sentiment_sad
        self.face_x = 0
        self.face_y = 0
        self.mouth_is_found = False
        self.mouth_image = None
        if sentiment_evaluator:
            self.evaluator = sentiment_evaluator

    def set_face_position(self, (x, y)):
        self.face_x = x
        self.face_y = y

    def detect(self, frame, frame_rgb):
        self.mouth_is_found = False
        mouths = self.mouth_cascade.detectMultiScale(frame, 1.3, 5)
        if( len(mouths)>0 ):
            self.mouth_is_found = True
            # search biggest mouth (maybe is the right)
            max_area = 0
            selected_mouth = {'x':0, 'y':0, 'w':0, 'h':0}
            for (x,y,w,h) in mouths:
                temp_area = w*h
                if temp_area > max_area and y > int(np.shape(frame)[1]/2):
                    temp_area = max_area
                    selected_mouth['x'] = x
                    selected_mouth['y'] = y
                    selected_mouth['w'] = w
                    selected_mouth['h'] = h
            # print mouthbox into colored frame
            cv2.rectangle(
                frame_rgb,
                (
                    self.face_x + selected_mouth['x'],
                    self.face_y + selected_mouth['y']
                ),
                (
                    self.face_x + selected_mouth['x']+selected_mouth['w'],
                    self.face_y + selected_mouth['y']+selected_mouth['h']
                ),
                (255,255,0),
                2
            )
            # save mouth image
            self.mouth_image = frame[
                selected_mouth['y'] : selected_mouth['y']+selected_mouth['h'],
                selected_mouth['x'] : selected_mouth['x']+selected_mouth['w']
                
            ]
        return self.mouth_is_found

    def evaluate_sentiment(self):
        self.sentiment = self.evaluator.get_sentiment(self.mouth_image)
        
    def print_sentiment(self, frame_rgb):
        self.evaluate_sentiment()
        cv2.putText(
            frame_rgb,
            self.sentiment,
            (
                self.face_x,
                max(self.face_y-10, 0)
            ),
            cv2.FONT_HERSHEY_PLAIN,
            2,
            255,
            2
        )

    def get_mouth(self):
        return self.mouth_image