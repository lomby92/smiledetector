import numpy as np
import cv2
import os
from sklearn.svm import LinearSVC

sentiment_sad = "sad"
sentiment_happy = "happy"

class sentiment_evaluator:

    def __init__(self):
        import warnings
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        # init sentiment
        self.sentiment = sentiment_sad
        # load training images list
        self.img_happy_list = [i for i in os.listdir(
            os.path.dirname(os.path.abspath(__name__))+"/data"
        ) if i[0]=='h']
        self.img_sad_list = [i for i in os.listdir(
            os.path.dirname(os.path.abspath(__name__))+"/data"
        ) if i[0]=='s']
        #self.img_happy = cv2.imread('data/happy.jpeg', 0)
        #self.img_sad = cv2.imread('data/sad.jpeg', 0)
        # init svm
        if self.img_happy_list is not None:
            self.svm = LinearSVC()
            self.train_svm()

    def train_svm(self):
        happy_tr = np.empty((5000,0))
        sad_tr = np.empty((5000,0))
        y = []
        for i in self.img_happy_list:
            img = cv2.imread('data/'+ i , 0)
            # normalize images to 100x50
            img = cv2.resize(img, (100,50), interpolation=cv2.INTER_AREA)
            # unroll entire image
            col = np.ravel(img)
            # reshape column
            col = np.reshape(col, (col.shape[0], 1))
            # push column to happy training
            happy_tr = np.concatenate((happy_tr, col), axis=1)
            # set output
            y.append(1)
        for i in self.img_sad_list:
            img = cv2.imread('data/'+ i, 0)
            # normalize images to 100x50
            img = cv2.resize(img, (100,50), interpolation=cv2.INTER_AREA)
            # unroll entire image
            col = np.ravel(img)
            # reshape column
            col = np.reshape(col, (col.shape[0], 1))
            # push column to sad training
            sad_tr = np.concatenate((sad_tr, col), axis=1)
            # set output
            y.append(-1)
        
        # concatenate datasets
        x = np.concatenate((happy_tr, sad_tr), axis=1)
        # transpose x for fit
        x = np.transpose(x)
        print "Ready to fit svm with training set:"
        print "x: ", x.shape
        print "y: ", len(y)
        self.svm.fit(x, y)
        print "Training is finished"


    def get_sentiment(self, mouth_image):
        if mouth_image.shape[0] > 0:
            self.img_target = cv2.resize(mouth_image, (100,50), interpolation=cv2.INTER_AREA)
            self.evaluate()
        return self.sentiment

    def evaluate(self):
        test = np.ravel(self.img_target)
        out = self.svm.predict(test)
        if out>0:
            self.sentiment = "happy"
        else:
            self.sentiment = "sad"