import numpy as np
import cv2

lower_hsv = np.array([0, 0, 0])
upper_hsv = np.array([255, 255, 200])

class hsv_filter:

    def __init__(self):
        pass

    def filtering_needed(self, frame_rgb):
        return False

    def filter(self, frame_rgb):
        # move to hsv color space
        frame_hsv = cv2.cvtColor(frame_rgb, cv2.COLOR_BGR2HSV)
        # filtering high exposition on frame
        mask = cv2.inRange(frame_hsv, lower_hsv, upper_hsv)
        frame_hsv = cv2.bitwise_and(frame_hsv,frame_hsv, mask= mask)
        return frame_hsv