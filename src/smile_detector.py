import numpy as np
import cv2

smile_classifier = 'config/haarcascade_smile.xml'

sentiment_happy = "Happy"
sentiment_sad = "Sad"

class smile_detector:

    def __init__(self):
        self.smile_cascade = cv2.CascadeClassifier(smile_classifier)
        self.sentiment = "???"
        self.face_x = 0
        self.face_y = 0

    def set_face_position(self, (x, y)):
        self.face_x = x
        self.face_y = y

    def detect(self, frame, frame_rgb):
        self.sentiment = sentiment_sad
        smiles = self.smile_cascade.detectMultiScale(frame, 1.3, 5)
        if( len(smiles)>0 ):
            self.sentiment = sentiment_happy
            # print sentiment as string on colored frame
        cv2.putText(
            frame_rgb,
            self.sentiment,
            (
                self.face_x,
                max(self.face_y-10, 0)
            ),
            cv2.FONT_HERSHEY_PLAIN,
            2,
            255,
            2
        )

    def get_sentiment(self):
        return self.sentiment