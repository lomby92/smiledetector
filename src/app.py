import numpy as np
import cv2

from hsv_filter import hsv_filter as HF
from face_detector import face_detector as FD
from mouth_detector import mouth_detector as MD
from sentiment_evaluator import sentiment_evaluator as SE

# script parameters
capture_width = 480
capture_height = 360
window_title = 'Smile detector'

# open video from webcam, exit if is not opened
capt = cv2.VideoCapture(0)
if not capt.isOpened():
    quit("Unable to get video from cam")

# set frame size
capt.set(3, capture_width)
capt.set(4, capture_height)

print "\n\nPress 'q' to exit"

# init functional classes
hsv_filter = HF()
face_detector = FD()
sentiment_evaluator = SE()
mouth_detector = MD(sentiment_evaluator)

# main loop
while(True):
    
    # capture single frame
    ret, frame_rgb = capt.read()

    # copy frame colored into frame var
    frame = frame_rgb

    # hsv filtering if is needed
    if hsv_filter.filtering_needed(frame_rgb):
        frame = hsv_filter.filter(frame)

    # move from rgb to grayscale
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # search face
    if face_detector.detect(frame, frame_rgb):

        # getting face-box
        face = face_detector.get_face()

        # set face position on mouth_detector
        mouth_detector.set_face_position(
            face_detector.get_face_position()
        )
        # search mouth
        if mouth_detector.detect(face, frame_rgb):
            # obtain face sentiment
            mouth_detector.print_sentiment(frame_rgb)

    # display frame
    cv2.imshow(window_title, frame_rgb)
    if face_detector.face_is_founded():
        cv2.imshow('Face', face)
    
    # wait for exit key
    if cv2.waitKey(50) & 0xFF == ord('q'):
        break

# release the capture
capt.release()
cv2.destroyAllWindows()