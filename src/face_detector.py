import numpy as np
import cv2

face_classifier = 'config/haarcascade_frontalface_default.xml'

class face_detector:

    def __init__(self):
        self.face_cascade = cv2.CascadeClassifier(face_classifier)
        self.face_is_found = False
        self.face = None
        self.face_x = 0
        self.face_y = 0

    def get_face_position(self):
        return (self.face_x, self.face_y)

    def detect(self, frame, frame_rgb):
        # detect frontal faces
        faces = self.face_cascade.detectMultiScale(frame, 1.3, 5)
        # check result faces
        self.face_is_found = False
        if len(faces) > 0:
            self.face_is_found = True
            # select biggest face
            max_area = 0
            selected_face = {'x':0, 'y':0, 'w':0, 'h':0}
            for (x,y,w,h) in faces:
                if (w*h > max_area):
                    selected_face['x'] = x
                    selected_face['y'] = y
                    selected_face['w'] = w
                    selected_face['h'] = h
            self.face_x = selected_face['x']
            self.face_y = selected_face['y']
            # add face box into frame colored
            cv2.rectangle(
                frame_rgb,
                (
                    selected_face['x'],
                    selected_face['y']
                ),
                (
                    selected_face['x']+selected_face['w'],
                    selected_face['y']+selected_face['h']
                ),
                (255,0,0),
                2
            )
            self.face = frame[         
                selected_face['y'] : selected_face['y']+selected_face['h'],
                selected_face['x'] : selected_face['x']+selected_face['w']
            ]
        return self.face_is_found

    def get_face(self):
        return self.face

    def face_is_founded(self):
        return self.face_is_found