import numpy as np
import cv2
import os

from hsv_filter import hsv_filter as HF
from face_detector import face_detector as FD
from mouth_detector import mouth_detector as MD

# script parameters
capture_width = 480
capture_height = 360
window_title = 'Smile detector - training'
filepath = "data/"
file_extension = ".jpeg"
happy_filename = "happy"
sad_filename = "sad"
filenumber_length = 8 # number of character used for file indexing

def number_to_string(n):
    temp = str(n)
    while(len(temp) < filenumber_length):
        temp = '0'+temp
    return temp

# find biggest number on happy and sad filenames
happy_filenumber = int(
    max(
        [i for i in os.listdir(
            os.path.dirname(os.path.abspath(__name__))+"/data"
        ) if i[0]=='h']
    )[len('happy'):len('happy')+filenumber_length]
)
sad_filenumber = int(
    max(
        [i for i in os.listdir(
            os.path.dirname(os.path.abspath(__name__))+"/data"
        ) if i[0]=='s']
    )[len('sad'):len('sad')+filenumber_length]
)

# open video from webcam, exit if is not opened
capt = cv2.VideoCapture(0)
if not capt.isOpened():
    quit("Unable to get video from cam")

# set frame size
capt.set(3, capture_width)
capt.set(4, capture_height)

print "Press: \n\t'q' to exit from training\n\t'h' to get a happy image\n\t's' to get a sad image"


# init functional classes
hsv_filter = HF()
face_detector = FD()
mouth_detector = MD(None)

# init photo_created value
happy_saved = False
sad_saved = False

# main loop
while(True):

    # get user input
    c = cv2.waitKey(33)
    
    # capture single frame
    ret, frame_rgb = capt.read()

    # copy frame colored into frame var
    frame = frame_rgb

    # hsv filtering if is needed
    if hsv_filter.filtering_needed(frame_rgb):
        frame = hsv_filter.filter(frame)

    # move from rgb to grayscale
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # search face
    if face_detector.detect(frame, frame_rgb):

        # getting face-box
        face = face_detector.get_face()

        # set face position on mouth_detector
        mouth_detector.set_face_position(
            face_detector.get_face_position()
        )
        # search mouth
        if mouth_detector.detect(face, frame_rgb):
            # obtain face sentiment
            #mouth_detector.print_sentiment(frame_rgb)
            mouth = mouth_detector.get_mouth()
            # save mouth image if 'h' or 's' pressed
            if c & 0xFF == ord('h'):
                happy_filenumber = happy_filenumber + 1
                cv2.imwrite(filepath + happy_filename + number_to_string(happy_filenumber) + file_extension, mouth)
                happy_saved = True
            if c & 0xFF == ord('s'):
                sad_filenumber = sad_filenumber + 1
                cv2.imwrite(filepath + sad_filename + number_to_string(sad_filenumber) + file_extension, mouth)
                sad_saved = True

    # display frame
    cv2.imshow(window_title, frame_rgb)
    if face_detector.face_is_founded():
        cv2.imshow('Face', face)
    
    # wait for exit key
    if c & 0xFF == ord('q'):
        break

    # check if happy and sad images is saved
    if happy_saved and sad_saved:
        print "Happy image and Sad image is saved, ready for recognize mode"
        break

# release the capture
capt.release()
cv2.destroyAllWindows()